pub use glfw::Key;
pub use glfw::MouseButton;

/// Input event.
pub enum InputEvent {
	/// Key pressed.
	KeyDown(Key),
	/// Key released.
	KeyUp(Key),
	/// Character typed.
	Char(char),
	/// Mouse button pressed.
	MouseDown(MouseButton),
	/// Mouse button released.
	MouseUp(MouseButton),
	/// Mouse moved.
	Mouse {x: u32, y: u32, prev_x: u32, prev_y: u32},
	/// Mouse scrolled. Horizontal and vertical offsets.
	Scroll {x: f64, y: f64}
}