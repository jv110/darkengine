use graphics::{Display, Renderer};
use audio::AudioContext;
use input::InputEvent;
use window::Window;

/// Arguments to `App::load`.
pub struct LoadArgs<'a> {
	/// App window.
	pub window: &'a mut Window,
	/// Graphics context, for loading assets.
	pub display: &'a Display,
	/// Audio context.
	pub audio: &'a AudioContext
}

/// Arguments to `App::update`.
pub struct UpdateArgs<'a> {
	/// App window.
	pub window: &'a mut Window,
	/// Audio context.
	pub audio: &'a AudioContext,
	/// Time since last frame, in seconds.
	pub delta: f64
}

/// Arguments to `App::render`.
pub struct RenderArgs<'a> {
	/// Rendering utility.
	pub renderer: &'a mut Renderer<'a>,
	/// Graphics context, for loading assets.
	pub display: &'a Display,
	/// Time since last frame, in seconds.
	pub delta: f64
}

/// Arguments to `App::close`.
pub struct CloseArgs<'a> {
	/// App window.
	pub window: &'a mut Window
}

/// Arguments to `App::input`.
pub struct InputArgs<'a> {
	/// App window.
	pub window: &'a mut Window,
	/// Graphics context, for loading assets.
	pub display: &'a Display,
	/// Audio context.
	pub audio: &'a AudioContext,
	/// The input event.
	pub event: InputEvent
}

/// Arguments to `App::resize`.
pub struct ResizeArgs<'a> {
	/// App window.
	pub window: &'a mut Window,
	/// Graphics context, for loading assets.
	pub display: &'a Display,
	/// Audio context.
	pub audio: &'a AudioContext,
	/// New window width.
	pub width: u32,
	/// New window height.
	pub height: u32
}

/// Arguments to `App::focus`.
pub struct FocusArgs<'a> {
	/// App window.
	pub window: &'a mut Window,
	/// Graphics context, for loading assets.
	pub display: &'a Display,
	/// Audio context.
	pub audio: &'a AudioContext,
	/// Whether the window is focused.
	pub focus: bool
}

/// The interface between the engine and your game. Implement it, and pass your struct to `Window::main_loop`.
pub trait App {
	/// Called after creation to load assets.
	fn load(&mut self, args: LoadArgs);
	/// Called every frame to update the game.
	fn update(&mut self, args: UpdateArgs);
	/// Called every frame to render the game.
	fn render(&mut self, args: RenderArgs);
	/// Called when the window's close button is pressed.
	fn close(&mut self, args: CloseArgs);
	/// Called on input events.
	fn input(&mut self, args: InputArgs) {let _ = args;}
	/// Called when the window is resized.
	fn resize(&mut self, args: ResizeArgs) {let _ = args;}
	/// Called when the window gains or loses focus.
	fn focus(&mut self, args: FocusArgs) {let _ = args;}
}