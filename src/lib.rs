//! 2D game engine providing graphics, audio and input.
//! 
//! It's in development, so it's subject to constant change. Features planned include:
//! - more efficient rendering for textures and dynamic fonts
//! - shaders
//! - 3D graphics
//! 
//! Physics are not included. You can use any library for that, or none.
//! 
//! # Example
//! ```rust,no_run
//! #[macro_use]
//! extern crate darkengine;
//! 
//! use darkengine::app::*;
//! use darkengine::window::*;
//! use darkengine::graphics::Texture;
//! use darkengine::input::{InputEvent, Key};
//! use darkengine::audio::{SoundStream, Sound};
//! 
//! struct Game {
//! 	tex: Option<Texture>,
//! 	music: Option<SoundStream>,
//! 	x: i32
//! }
//! 
//! impl App for Game {
//! 	fn load(&mut self, args: LoadArgs) {
//! 		self.tex = Some(Texture::load(canon_str!("assets/test.png"), args.display).unwrap());
//! 		let mut music = SoundStream::load(canon_str!("assets/music.ogg"), args.audio).unwrap();
//! 		music.play(args.audio);
//! 		self.music = Some(music);
//! 	}
//! 
//! 	fn update(&mut self, args: UpdateArgs) {
//! 		if args.window.is_key_pressed(Key::A) {
//! 			self.x -= 10;
//! 		}
//! 		if args.window.is_key_pressed(Key::D) {
//! 			self.x += 10;
//! 		}
//! 	}
//! 
//! 	fn render(&mut self, args: RenderArgs) {
//! 		args.renderer.draw_texture(self.tex.as_ref().unwrap(), 300 + self.x, 100);
//! 		args.renderer.draw_text("Example", 20, 20, 1.0, 1.0, 1.0, 1.0);
//! 	}
//! 	
//! 	fn input(&mut self, args: InputArgs) {
//! 		match args.event {
//! 			InputEvent::KeyDown(Key::Escape) => {
//! 				args.window.stop();
//! 			},
//! 			_ => {}
//! 		}
//! 	}
//! 
//! 	fn close(&mut self, args: CloseArgs) {
//! 		args.window.stop();
//! 	}
//! }
//! 
//! fn main() {
//! 	let mut window = Window::new(WindowDef {
//! 		title: "Example".to_owned(),
//! 		width: 640,
//! 		height: 480,
//! 		.. Default::default()
//! 	});
//! 	window.main_loop(&mut Game {tex: None, music: None, x: 0});
//! }
//! ```

#[macro_use]
extern crate glium;
extern crate glfw;
extern crate image;
extern crate cgmath;
extern crate ears;
extern crate rusttype;
extern crate bmfont;

pub mod app;
pub mod graphics;
pub mod input;
pub mod audio;
pub mod window;
mod glfw_backend;

/// Canonicalize string path. Panics if invalid.
#[macro_export]
macro_rules! canon_str {
	($s:expr) => {{
		use std;
		std::fs::canonicalize(std::path::Path::new($s)).unwrap().as_path()
	}};
}
