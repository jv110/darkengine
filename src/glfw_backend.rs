use std::rc::Rc;
use std::cell::RefCell;
use std::os::raw::c_void;
use glfw::{self, Context};
use glium;

pub(crate) struct GlfwBackend {
	window: Rc<RefCell<glfw::Window>>
}

impl GlfwBackend {
	pub(crate) fn new(window: Rc<RefCell<glfw::Window>>) -> GlfwBackend {
		GlfwBackend {window}
	}
}

unsafe impl glium::backend::Backend for GlfwBackend {
	fn swap_buffers(&self) -> Result<(), glium::SwapBuffersError> {
		self.window.borrow_mut().swap_buffers();
		Ok(())
	}

	unsafe fn get_proc_address(&self, symbol: &str) -> *const c_void {
		self.window.borrow_mut().get_proc_address(symbol) as *const _
	}

	fn get_framebuffer_dimensions(&self) -> (u32, u32) {
		let (w, h) = self.window.borrow().get_framebuffer_size();
		(w as u32, h as u32)
	}

	fn is_current(&self) -> bool {
		self.window.borrow().is_current()
	}

	unsafe fn make_current(&self) {
        self.window.borrow_mut().make_current();
	}
}

/// The graphics context.
#[derive(Clone)]
pub struct Display {
	context: Rc<glium::backend::Context>
}

impl Display {
	pub(crate) fn new(context: Rc<glium::backend::Context>) -> Display {
		Display {context}
	}
}

impl glium::backend::Facade for Display {
	fn get_context(&self) -> &Rc<glium::backend::Context> {
		&self.context
	}
}