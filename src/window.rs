use std::rc::Rc;
use std::cell::RefCell;
use std::time::Instant;
use std::sync::mpsc::Receiver;
use glfw::{self, Context, Action};
use glfw_backend::GlfwBackend;
use glium::{self, backend::Facade};
use graphics::{Display, Renderer, Font, Vertex};
use audio::AudioContext;
use input::*;
use app::*;

implement_vertex!(Vertex, pos, uv);

/// Information to build a window.
pub struct WindowDef {
	/// Window title.
	pub title: String,
	/// Window width.
	pub width: u32,
	/// Window height.
	pub height: u32,
	/// Whether the window is fullscreen.
	pub fullscreen: bool,
	/// Whether the window is resizable. Ignored if fullscreen is set.
	pub resizable: bool
}

impl Default for WindowDef {
	fn default() -> WindowDef {
		WindowDef {
			title: "".to_owned(),
			width: 0,
			height: 0,
			fullscreen: false,
			resizable: false
		}
	}
}

/// A window. Also used for receiving input.
pub struct Window {
	prev_mx: u32,
	prev_my: u32,
	running: bool,
	should_stop: bool,
	glfw: glfw::Glfw,
	events: Receiver<(f64, glfw::WindowEvent)>,
	window: Rc<RefCell<glfw::Window>>,
	audio: AudioContext,
	display: Display,
	quad: glium::VertexBuffer<Vertex>,
	tex_program: glium::Program,
	rect_program: glium::Program,
	text_program: glium::Program,
	default_fonts: Vec<(u32, Rc<Font>)>
}

impl Window {
	/// Create a window.
	pub fn new(def: WindowDef) -> Window {
		let mut glfw = glfw::init(glfw::FAIL_ON_ERRORS).unwrap();
		glfw.window_hint(glfw::WindowHint::ClientApi(glfw::ClientApiHint::OpenGlEs));
		glfw.window_hint(glfw::WindowHint::ContextVersion(2, 0));
		glfw.window_hint(glfw::WindowHint::Resizable(def.resizable));
		let (mut window, events) = if def.fullscreen {
			glfw.with_primary_monitor(|glfw, m| {
				glfw.create_window(def.width, def.height, def.title.as_str(), glfw::WindowMode::FullScreen(m.unwrap()))
			})
		} else {
			glfw.create_window(def.width, def.height, def.title.as_str(), glfw::WindowMode::Windowed)
		}.expect("error creating a GLFW window");
		window.set_key_polling(true);
		window.set_char_polling(true);
		window.set_mouse_button_polling(true);
		window.set_cursor_pos_polling(true);
		window.set_scroll_polling(true);
		window.set_focus_polling(true);
		window.set_framebuffer_size_polling(true);
		window.make_current();
		let window = Rc::new(RefCell::new(window));
		let context = unsafe {
			glium::backend::Context::new(GlfwBackend::new(window.clone()), false, Default::default())
		}.unwrap();
		let display = Display::new(context);
		let quad = glium::VertexBuffer::new(&display, &[
			Vertex {pos: [0.0, 0.0], uv: [0.0, 0.0]},
			Vertex {pos: [1.0, 0.0], uv: [1.0, 0.0]},
			Vertex {pos: [1.0, 1.0], uv: [1.0, 1.0]},
			Vertex {pos: [0.0, 1.0], uv: [0.0, 1.0]}
		]).unwrap();
		let rect_program = glium::Program::from_source(&display, r#"
attribute vec2 pos;
uniform vec2 size;
uniform mat4 mvp;
void main() {
	gl_Position = mvp * vec4(pos * size, 0.0, 1.0);
}
		"#, r#"
uniform highp vec4 color;
void main() {
	gl_FragColor = color;
}
		"#, None).unwrap();
		let tex_program = glium::Program::from_source(&display, r#"
attribute vec2 pos;
attribute vec2 uv;
uniform vec2 size;
uniform vec4 region;
uniform mat4 mvp;
varying vec2 uv_;
void main() {
	uv_ = uv * region.zw + (vec2(1.0, 1.0) - uv) * region.xy;
	gl_Position = mvp * vec4(pos * size, 0.0, 1.0);
}
		"#, r#"
uniform highp vec4 color;
uniform sampler2D tex;
varying mediump vec2 uv_;
void main() {
	gl_FragColor = texture2D(tex, uv_) * color;
}
		"#, None).unwrap();
		let text_program = glium::Program::from_source(&display, r#"
attribute vec2 pos;
attribute vec2 uv;
uniform vec2 size;
uniform mat4 mvp;
varying vec2 uv_;
void main() {
	uv_ = uv;
	gl_Position = mvp * vec4(pos * size, 0.0, 1.0);
}
		"#, r#"
uniform highp vec4 color;
uniform sampler2D tex;
varying mediump vec2 uv_;
void main() {
	gl_FragColor = color * texture2D(tex, uv_).x;
}
		"#, None).unwrap();
		Window {
			prev_mx: 0,
			prev_my: 0,
			running: false,
			should_stop: false,
			glfw,
			events,
			window,
			audio: AudioContext,
			display,
			quad,
			tex_program,
			rect_program,
			text_program,
			default_fonts: Vec::new()
		}
	}

	/// Start the main loop.
	pub fn main_loop<'b>(&'b mut self, app: &mut App) {
		if self.running {
			panic!("called main_loop, but window is already running");
		}
		self.running = true;
		let mut last_update = Instant::now();
		let display = self.display.clone();
		let audio = self.audio.clone();
		let context = display.get_context();
		{
			app.load(LoadArgs {
				window: self,
				display: &display,
				audio: &audio
			});
		}
		while self.running {
			self.glfw.poll_events();
			if self.window.borrow().should_close() {
				app.close(CloseArgs {
					window: self
				});
			}
			if self.should_stop {
				break;
			}
			let mut events = Vec::new();
			for (_, event) in glfw::flush_messages(&self.events) {
				events.push(event);
			}
			for event in events {
				match event {
					glfw::WindowEvent::Key(key, _, Action::Press, _) => {
						app.input(InputArgs {
							window: self,
							display: &display,
							audio: &audio,
							event: InputEvent::KeyDown(key)
						});
					},
					glfw::WindowEvent::Key(key, _, Action::Release, _) => {
						app.input(InputArgs {
							window: self,
							display: &display,
							audio: &audio,
							event: InputEvent::KeyUp(key)
						});
					},
					glfw::WindowEvent::MouseButton(button, Action::Press, _) => {
						app.input(InputArgs {
							window: self,
							display: &display,
							audio: &audio,
							event: InputEvent::MouseDown(button)
						});
					},
					glfw::WindowEvent::MouseButton(button, Action::Release, _) => {
						app.input(InputArgs {
							window: self,
							display: &display,
							audio: &audio,
							event: InputEvent::MouseUp(button)
						});
					},
					glfw::WindowEvent::Char(c) => {
						app.input(InputArgs {
							window: self,
							display: &display,
							audio: &audio,
							event: InputEvent::Char(c)
						});
					},
					glfw::WindowEvent::CursorPos(x, y) => {
						let prev_mx = self.prev_mx;
						let prev_my = self.prev_my;
						app.input(InputArgs {
							window: self,
							display: &display,
							audio: &audio,
							event: InputEvent::Mouse {
								x: x as u32,
								y: y as u32,
								prev_x: prev_mx,
								prev_y: prev_my
							}
						});
						self.prev_mx = x as u32;
						self.prev_my = y as u32;
					}
					glfw::WindowEvent::FramebufferSize(w, h) => {
						app.resize(ResizeArgs {
							window: self,
							display: &display,
							audio: &audio,
							width: w as u32,
							height: h as u32
						});
					},
					_ => {}
				}
			}
			let now = Instant::now();
			let diff = now - last_update;
			let delta = (diff.as_secs() * 1000 + diff.subsec_millis() as u64) as f64 / 1000f64;
			last_update = now;
			app.update(UpdateArgs {
				window: self,
				audio: &audio,
				delta
			});
			let (w, h) = self.get_size();
			let mut target = glium::Frame::new(context.clone(), (w, h));
			{
				let mut renderer = Renderer::new(
					&display,
					&mut target,
					&self.quad,
					&self.tex_program,
					&self.rect_program,
					&self.text_program,
					&mut self.default_fonts,
					w,
					h
				);
				renderer.clear();
				app.render(RenderArgs {
					renderer: &mut renderer,
					display: &display,
					delta
				});
			}
			target.finish().unwrap();
		}
		self.running = false;
	}

	/// Stop running.
	pub fn stop(&mut self) {
		self.should_stop = true;
	}

	/// Get size in pixels.
	pub fn get_size(&self) -> (u32, u32) {
		let (w, h) = self.window.borrow().get_framebuffer_size();
		(w as u32, h as u32)
	}

	/// Set size in pixels.
	pub fn set_size(&self, w: u32, h: u32) {
		self.window.borrow_mut().set_size(w as i32, h as i32);
	}

	/// Whether a key is pressed.
	pub fn is_key_pressed(&self, key: Key) -> bool {
		self.window.borrow().get_key(key) == Action::Press
	}

	/// Whether a key is released.
	pub fn is_key_released(&self, key: Key) -> bool {
		self.window.borrow().get_key(key) == Action::Release
	}

	/// Whether a mouse button is pressed.
	pub fn is_mouse_pressed(&self, button: MouseButton) -> bool {
		self.window.borrow().get_mouse_button(button) == Action::Press
	}

	/// Whether a mouse button is released.
	pub fn is_mouse_released(&self, button: MouseButton) -> bool {
		self.window.borrow().get_mouse_button(button) == Action::Release
	}
}
