use super::BitmapFont;
use super::DynamicFont;

/// A generic font.
pub enum Font {
	Dynamic(DynamicFont),
	Bitmap(BitmapFont)
}