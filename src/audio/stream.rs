use std::path::Path;
use ears::{self, AudioController, AudioTags};
use super::{Sound, State, AudioContext};

/// An audio stream. Use for long sounds, like music.
pub struct SoundStream {
	music: ears::Music
}

impl SoundStream {
	/// Create a sound stream from a file.
	/// # Example
	/// ```rust,no_run
	/// # #[macro_use]
	/// # extern crate darkengine;
	/// # use darkengine::audio::{SoundStream, Sound};
	/// # fn main() {
	/// # let audio_context = None.unwrap();
	/// let mut stream = SoundStream::load(canon_str!("music.ogg"), &audio_context).unwrap();
	/// # }
	/// ```
	pub fn load(path: &Path, context: &AudioContext) -> Result<SoundStream, ()> {
		let _ = context;
		match ears::Music::new(path.to_str().unwrap()) {
			Ok(music) => Ok(SoundStream {music}),
			Err(_) => Err(())
		}
	}
}

impl Sound for SoundStream {
	fn play(&mut self, _: &AudioContext) {
		self.music.play();
	}

	fn pause(&mut self, _: &AudioContext) {
		self.music.pause();
	}

	fn stop(&mut self, _: &AudioContext) {
		self.music.stop();
	}

	fn is_playing(&self, _: &AudioContext) -> bool {
		self.music.is_playing()
	}

	fn state(&self, _: &AudioContext) -> State {
		self.music.get_state()
	}

	fn set_volume(&mut self, volume: f32, _: &AudioContext) {
		self.music.set_volume(volume);
	}

	fn set_min_volume(&mut self, volume: f32, _: &AudioContext) {
		self.music.set_min_volume(volume);
	}

	fn set_position(&mut self, x: f32, y: f32, z: f32, _: &AudioContext) {
		self.music.set_position([x, y, z]);
	}

	fn position(&self, _: &AudioContext) -> (f32, f32, f32) {
		let pos = self.music.get_position();
		(pos[0], pos[1], pos[2])
	}

	fn set_relative(&mut self, relative: bool, _: &AudioContext) {
		self.music.set_relative(relative);
	}

	fn is_relative(&mut self, _: &AudioContext) -> bool {
		self.music.is_relative()
	}

	fn set_reference_distance(&mut self, distance: f32, _: &AudioContext) {
		self.music.set_reference_distance(distance);
	}

	fn reference_distance(&self, _: &AudioContext) -> f32 {
		self.music.get_reference_distance()
	}

	fn set_max_distance(&mut self, distance: f32, _: &AudioContext) {
		self.music.set_max_distance(distance);
	}

	fn max_distance(&self, _: &AudioContext) -> f32 {
		self.music.get_max_distance()
	}

	fn set_direction(&mut self, x: f32, y: f32, z: f32, _: &AudioContext) {
		self.music.set_direction([x, y, z]);
	}

	fn direction(&self, _: &AudioContext) -> (f32, f32, f32) {
		let dir = self.music.get_direction();
		(dir[0], dir[1], dir[2])
	}

	fn set_attenuation(&mut self, attenuation: f32, _: &AudioContext) {
		self.music.set_attenuation(attenuation);
	}

	fn attenuation(&self, _: &AudioContext) -> f32 {
		self.music.get_attenuation()
	}

	fn set_looping(&mut self, looping: bool) {
		self.music.set_looping(looping);
	}

	fn is_looping(&self) -> bool {
		self.music.is_looping()
	}

	fn title(&self) -> Option<String> {
		let tag = self.music.get_tags().title;
		if tag == "" {
			None
		} else {
			Some(tag)
		}
	}

	fn copyright(&self) -> Option<String> {
		let tag = self.music.get_tags().copyright;
		if tag == "" {
			None
		} else {
			Some(tag)
		}
	}

	fn software(&self) -> Option<String> {
		let tag = self.music.get_tags().software;
		if tag == "" {
			None
		} else {
			Some(tag)
		}
	}

	fn artist(&self) -> Option<String> {
		let tag = self.music.get_tags().artist;
		if tag == "" {
			None
		} else {
			Some(tag)
		}
	}

	fn comment(&self) -> Option<String> {
		let tag = self.music.get_tags().comment;
		if tag == "" {
			None
		} else {
			Some(tag)
		}
	}

	fn date(&self) -> Option<String> {
		let tag = self.music.get_tags().date;
		if tag == "" {
			None
		} else {
			Some(tag)
		}
	}

	fn album(&self) -> Option<String> {
		let tag = self.music.get_tags().album;
		if tag == "" {
			None
		} else {
			Some(tag)
		}
	}

	fn license(&self) -> Option<String> {
		let tag = self.music.get_tags().license;
		if tag == "" {
			None
		} else {
			Some(tag)
		}
	}

	fn track_number(&self) -> Option<String> {
		let tag = self.music.get_tags().track_number;
		if tag == "" {
			None
		} else {
			Some(tag)
		}
	}

	fn genre(&self) -> Option<String> {
		let tag = self.music.get_tags().genre;
		if tag == "" {
			None
		} else {
			Some(tag)
		}
	}
}