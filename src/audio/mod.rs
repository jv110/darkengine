pub use ears::State;
mod stream;
pub use self::stream::SoundStream;
mod sample;
pub use self::sample::SoundSample;
mod context;
pub use self::context::AudioContext;

// ears only supports a global context
// the context is faked because there will be an actual
// context in the future like there is with graphics

/// Generic interface to a sound stream or sample.
pub trait Sound {
	/// Begin playing.
	fn play(&mut self, context: &AudioContext);
	/// Pause.
	fn pause(&mut self, context: &AudioContext);
	/// Stop playing.
	fn stop(&mut self, context: &AudioContext);
	/// Whether the sound is currently playing.
	fn is_playing(&self, context: &AudioContext) -> bool;
	/// The state of the sound.
	fn state(&self, context: &AudioContext) -> State;
	/// Set the volume.
	fn set_volume(&mut self, volume: f32, context: &AudioContext);
	/// Set the minimum volume, after attenuation is applied.
	fn set_min_volume(&mut self, volume: f32, context: &AudioContext);
	/// Set the position. (0, 0, 0) is the origin, X is right, Y is up, Z is back.
	fn set_position(&mut self, x: f32, y: f32, z: f32, context: &AudioContext);
	/// Position.
	fn position(&self, context: &AudioContext) -> (f32, f32, f32);
	/// Set whether the position is relative to the listener or an absolute position in the world.
	fn set_relative(&mut self, relative: bool, context: &AudioContext);
	/// Whether the position is relative to the listener.
	fn is_relative(&mut self, context: &AudioContext) -> bool;
	/// Set the reference distance. Being any closer than this will make it play no louder than being at this distance. Defaults to 1.0.
	fn set_reference_distance(&mut self, distance: f32, context: &AudioContext);
	/// Reference distance.
	fn reference_distance(&self, context: &AudioContext) -> f32;
	/// Set the maximum distance from the listener. Being any farther than this will make it play no softer than being at this distance. Defaults to +inf.
	fn set_max_distance(&mut self, distance: f32, context: &AudioContext);
	/// Maximum distance from the listener.
	fn max_distance(&self, context: &AudioContext) -> f32;
	/// Set the direction, a normal vector.
	fn set_direction(&mut self, x: f32, y: f32, z: f32, context: &AudioContext);
	/// Direction.
	fn direction(&self, context: &AudioContext) -> (f32, f32, f32);
	/// Set the distance attenuation, from 0.0 to 1.0. Defaults to 1.0.
	fn set_attenuation(&mut self, attenuation: f32, context: &AudioContext);
	/// Distance attenuation.
	fn attenuation(&self, context: &AudioContext) -> f32;
	/// Set whether the sound loops.
	fn set_looping(&mut self, looping: bool);
	/// Whether the sound loops.
	fn is_looping(&self) -> bool;
	/// Title, if present in the sound file.
	fn title(&self) -> Option<String>;
	/// Copyright, if present in the sound file.
	fn copyright(&self) -> Option<String>;
	/// Software used to create the sound, if present in the sound file.
	fn software(&self) -> Option<String>;
	/// Artist, if present in the sound file.
	fn artist(&self) -> Option<String>;
	/// Comment, if present in the sound file.
	fn comment(&self) -> Option<String>;
	/// Date, if present in the sound file.
	fn date(&self) -> Option<String>;
	/// Album, if present in the sound file.
	fn album(&self) -> Option<String>;
	/// License, if present in the sound file.
	fn license(&self) -> Option<String>;
	/// Track number, if present in the sound file.
	fn track_number(&self) -> Option<String>;
	/// Genre, if present in the sound file.
	fn genre(&self) -> Option<String>;
}