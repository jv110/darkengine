use ears::listener;

/// Audio context.
#[derive(Clone)]
pub struct AudioContext;

impl AudioContext {
	/// Set listener position.
	pub fn set_position(&mut self, x: f32, y: f32, z: f32) {
		listener::set_position([x, y, z]);
	}

	/// Set listener orientation with forward and up normals. Defaults to forward (0, 0, -1) and up (0, 1, 0).
	pub fn set_orientation(&mut self, fw_x: f32, fw_y: f32, fw_z: f32, up_x: f32, up_y: f32, up_z: f32) {
		listener::set_orientation([fw_x, fw_y, fw_z], [up_x, up_y, up_z]);
	}

	/// Set global volume.
	pub fn set_volume(&mut self, volume: f32) {
		listener::set_volume(volume);
	}

	/// The listener position.
	pub fn position(&self) -> (f32, f32, f32) {
		let pos = listener::get_position();
		(pos[0], pos[1], pos[2])
	}

	/// The listener orientation in forward and up normals.
	pub fn orientation(&self) -> (f32, f32, f32, f32, f32, f32) {
		let (fw, up) = listener::get_orientation();
		(fw[0], fw[1], fw[2], up[0], up[1], up[2])
	}

	/// The global volume.
	pub fn volume(&self) -> f32 {
		listener::get_volume()
	}
}