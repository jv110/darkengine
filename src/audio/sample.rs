use std::path::Path;
use ears::{self, AudioController, AudioTags};
use super::{Sound, State, AudioContext};

/// An audio sample, unpacked in memory. Use for short sounds. Large sounds played as a sample may cause memory problems.
pub struct SoundSample {
	sound: ears::Sound
}

impl SoundSample {
	/// Create a sound sample from a file.
	/// # Example
	/// ```rust,no_run
	/// # #[macro_use]
	/// # extern crate darkengine;
	/// # use darkengine::audio::{SoundSample, Sound};
	/// # fn main() {
	/// # let audio_context = None.unwrap();
	/// let mut sample = SoundSample::load(canon_str!("sound.ogg"), &audio_context).unwrap();
	/// # }
	/// ```
	pub fn load(path: &Path, context: &AudioContext) -> Result<SoundSample, ()> {
		let _ = context;
		match ears::Sound::new(path.to_str().unwrap()) {
			Ok(sound) => Ok(SoundSample {sound}),
			Err(_) => Err(())
		}
	}
}

impl Sound for SoundSample {
	fn play(&mut self, _: &AudioContext) {
		self.sound.play();
	}

	fn pause(&mut self, _: &AudioContext) {
		self.sound.pause();
	}

	fn stop(&mut self, _: &AudioContext) {
		self.sound.stop();
	}

	fn is_playing(&self, _: &AudioContext) -> bool {
		self.sound.is_playing()
	}

	fn state(&self, _: &AudioContext) -> State {
		self.sound.get_state()
	}

	fn set_volume(&mut self, volume: f32, _: &AudioContext) {
		self.sound.set_volume(volume);
	}

	fn set_min_volume(&mut self, volume: f32, _: &AudioContext) {
		self.sound.set_min_volume(volume);
	}

	fn set_position(&mut self, x: f32, y: f32, z: f32, _: &AudioContext) {
		self.sound.set_position([x, y, z]);
	}

	fn position(&self, _: &AudioContext) -> (f32, f32, f32) {
		let pos = self.sound.get_position();
		(pos[0], pos[1], pos[2])
	}

	fn set_relative(&mut self, relative: bool, _: &AudioContext) {
		self.sound.set_relative(relative);
	}

	fn is_relative(&mut self, _: &AudioContext) -> bool {
		self.sound.is_relative()
	}

	fn set_reference_distance(&mut self, distance: f32, _: &AudioContext) {
		self.sound.set_reference_distance(distance);
	}

	fn reference_distance(&self, _: &AudioContext) -> f32 {
		self.sound.get_reference_distance()
	}

	fn set_max_distance(&mut self, distance: f32, _: &AudioContext) {
		self.sound.set_max_distance(distance);
	}

	fn max_distance(&self, _: &AudioContext) -> f32 {
		self.sound.get_max_distance()
	}

	fn set_direction(&mut self, x: f32, y: f32, z: f32, _: &AudioContext) {
		self.sound.set_direction([x, y, z]);
	}

	fn direction(&self, _: &AudioContext) -> (f32, f32, f32) {
		let dir = self.sound.get_direction();
		(dir[0], dir[1], dir[2])
	}

	fn set_attenuation(&mut self, attenuation: f32, _: &AudioContext) {
		self.sound.set_attenuation(attenuation);
	}

	fn attenuation(&self, _: &AudioContext) -> f32 {
		self.sound.get_attenuation()
	}

	fn set_looping(&mut self, looping: bool) {
		self.sound.set_looping(looping);
	}

	fn is_looping(&self) -> bool {
		self.sound.is_looping()
	}

	fn title(&self) -> Option<String> {
		let tag = self.sound.get_tags().title;
		if tag == "" {
			None
		} else {
			Some(tag)
		}
	}

	fn copyright(&self) -> Option<String> {
		let tag = self.sound.get_tags().copyright;
		if tag == "" {
			None
		} else {
			Some(tag)
		}
	}

	fn software(&self) -> Option<String> {
		let tag = self.sound.get_tags().software;
		if tag == "" {
			None
		} else {
			Some(tag)
		}
	}

	fn artist(&self) -> Option<String> {
		let tag = self.sound.get_tags().artist;
		if tag == "" {
			None
		} else {
			Some(tag)
		}
	}

	fn comment(&self) -> Option<String> {
		let tag = self.sound.get_tags().comment;
		if tag == "" {
			None
		} else {
			Some(tag)
		}
	}

	fn date(&self) -> Option<String> {
		let tag = self.sound.get_tags().date;
		if tag == "" {
			None
		} else {
			Some(tag)
		}
	}

	fn album(&self) -> Option<String> {
		let tag = self.sound.get_tags().album;
		if tag == "" {
			None
		} else {
			Some(tag)
		}
	}

	fn license(&self) -> Option<String> {
		let tag = self.sound.get_tags().license;
		if tag == "" {
			None
		} else {
			Some(tag)
		}
	}

	fn track_number(&self) -> Option<String> {
		let tag = self.sound.get_tags().track_number;
		if tag == "" {
			None
		} else {
			Some(tag)
		}
	}

	fn genre(&self) -> Option<String> {
		let tag = self.sound.get_tags().genre;
		if tag == "" {
			None
		} else {
			Some(tag)
		}
	}
}